############
System Check
############

Checks the correct setup and provides some system information.

Installation
------------

Install the package directly from the gitlab repository by

.. code:: bash

   pip install git+https://gitlab.com/dlab-indecol/iedl_checks.git --upgrade

Usage
-----

IEDL_checks for a consistent programming environment across
the IndEcol group (as described in the IndEcol wiki here
https://wiki.indecol.no/doku.php/indecol_digital_work_environment )

To run the check, install the program as explained above and run

.. code:: bash

   iedl_checks


The last line of the output will than either give you an OK (all programs installed as expected) or
a warning with some further information for how to further proceed.

Advanced usage
^^^^^^^^^^^^^^^

The command 'iedl_checks' without paramters is
equivalent to:

.. code:: bash

   iedl_checks -s
   iedl_checks --summary

For an full system overview do

.. code:: bash

   iedl_checks -f
   iedl_checks --full

The help is available with

.. code:: bash

   iedl_checks -h
   iedl_checks --help


Development and Contributing
-----------------------------

Please use the Issue Tracker for feedback, bug reports and suggestions for improvement.
Also file an issue describing planned changes before starting to contribute.

Extending the list of checked programs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For extending the list of checked programs, add the program to 
the top level dict 'PROGRAMS' in ./iedl_checks/checks.py .

Any new entry must have the form

.. code:: python

   'prog_name': {
        'cmd': 'prog command line/start command',
        'cmd_version': 'prog command to retrieve version',
            }

If the program does not provide a command line for retrieving a
version, pass None

.. code:: python

   'cmd_version': None

Extending the system checks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

System checks are defined in the top level dict 'SYSTEM_DATA_CHECKS'.

Use the format

.. code:: python

   'Name of check': function

You can use lambda expressions if parameters are needed.

Extending the list of checked environmental variables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These are defined in the top-level dict 'ENVIRONMENTAL_VARIABLES'.
The dict has two entries, one for 'linux' and one for 'Windows'. The
latter is used when a MS Windows system is detected, otherwise it
falls back to the linux list (should also work for OSx).
