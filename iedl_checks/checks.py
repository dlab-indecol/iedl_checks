""" IEDL System checks

To extend the list of check, see
../README.rst
"""

import platform
import psutil
import getpass
import os
import argparse
import subprocess as sp
from pathlib import Path
import pprint
from collections import namedtuple
from iedl_checks.version import __version__


PROGRAMS = {
    'Git': {
        'cmd': 'git',
        'cmd_version': 'git --version',
            },
    'Standard python': {
        'cmd': 'python',
        'cmd_version': 'python --version',
            },
    'IPython': {
        'cmd': 'ipython',
        'cmd_version': 'ipython --version',
            },
    'Jupyter': {
        'cmd': 'jupyter',
        'cmd_version': 'jupyter --version',
            },
    'Anaconda': {
        'cmd': 'anaconda',
        'cmd_version': 'anaconda --version',
            },
    'Conda': {
        'cmd': 'conda',
        'cmd_version': 'conda --version',
            },
    'Matlab': {
        'cmd': 'matlab',
        'cmd_version': None,
            },
}


ENVIRONMENT_VARIABLES = {
    'linux': ['HOME', 'PATH', 'USER', 'EDITOR',
              'TERM', 'SHELL', 'PWD', 'LANG', 'BROWSER'],
    'Windows': ['HOMESHARE', 'PATH', 'USERNAME', 'ComSpec',
                'OS', 'PATHEXT', 'WINDIR', 'TMP', 'TEMP',
                'NO_SEP']}


SYSTEM_DATA_CHECKS = {
    # 'User': psutil.users,
    'CPU Count': psutil.cpu_count,
    'CPU Freq': psutil.cpu_freq,
    'CPU Usage': psutil.cpu_percent,
    'CPU Times': psutil.cpu_times,
    'RAM': psutil.virtual_memory,
    'SWAP': psutil.swap_memory,
    'Disk Partitions': psutil.disk_partitions,
    'Disk Usage (current drive)': lambda: psutil.disk_usage('.'),
    'Network Addresses': psutil.net_if_addrs,
    'Network Stats': psutil.net_if_stats,
}


MISSING_PROG_IDENT = 'WARNING'


def get_system_info():
    """ Basic system info as defined in SYSTEM_DATA_CHECKS
    """
    fd = dict()
    for name, func in SYSTEM_DATA_CHECKS.items():
        try:
            fd[name] = func()
        except Exception as e:
            fd[name] = 'Exception: {}'.format(e)

    return fd


def installed_prog_locations(prog_dict):
    """ Retrieves version and location of installed programms

    Parameters
    ----------
    prog_dict: dict
        Dictionary of programms to check.
        prog_name: {cmd, cmd_version} - see top-level
        PROGRAMS for an example

    Returns
    -------
    prog_info_list: list of named tuples with infos of programms

    """

    if platform.system() == 'Windows':
        search_cmd = "where"
    else:
        search_cmd = "which"

    prog_info_list = list()
    prog_info = namedtuple('installed_program', ['name',
                                                 'location',
                                                 'version'])
    for name in prog_dict.keys():
        try:
            location = Path(sp.check_output(
                [search_cmd, prog_dict[name]['cmd']], stderr=sp.STDOUT).
                            decode("utf-8").
                            replace('\r', '').
                            replace('\n', ' '))
        except (FileNotFoundError, sp.CalledProcessError):
            location = '{}: Program CLI "{}" not found'.format(
                MISSING_PROG_IDENT, prog_dict[name]['cmd'])

        try:
            version = (sp.check_output(prog_dict[name]['cmd_version'].
                                       split(' '), stderr=sp.STDOUT).
                       decode("utf-8").
                       replace('\r', '').
                       replace('\n', ''))
        except (FileNotFoundError, sp.CalledProcessError, AttributeError):
            version = 'Identification of version not possible.'
        prog_info_list.append(prog_info(name=name,
                                        location=location,
                                        version=version))

    return prog_info_list


def get_environment_variables(env_var_list):
    """ Retrieve values of environmental variables

    Parameters
    ----------
    env_var_list: list of str
        Environmental Variables to check

    Returns
    --------
    list
        of named tuples with environmental variable and value

    """
    env_info = namedtuple('environment_variables', ['name', 'value'])
    return [env_info(name=vv, value=os.environ.get(vv, 'NOT SET')) for
            vv in env_var_list]


def print_basic_info():
    print('Basic system information:\n')
    print('System: {}'.format(platform.system()))
    print('Release: {}'.format(platform.release()))
    print('Version: {}'.format(platform.version()))
    print('Machine: {}'.format(platform.machine()))


def print_installed_programs(installed_prog):
    print('Checking installed programs of the Digital Work Environment:\n')
    missing_prog = []
    for p in installed_prog:
        print('{name}: \n\t{loc} - {ver}'.format(
            name=p.name, ver=p.version, loc=p.location))
        if MISSING_PROG_IDENT in str(p.location):
            missing_prog.append(p.name)
    if missing_prog:
        print('\nWARNING - NOT ALL EXPECTED PROGRAMS FOUND\n\n'
              '\tPlease double check if you exactly followed the \n'
              '\tinstallation guide for the Digital Work Environment \n'
              '\tas described in the IndEcol wiki.\n\n'
              '\tIf you than still encounter problems please run\n'
              '\tiedl_checks -f\n'
              '\tand email the output to the Digital Lab.\n'
              )
    else:
        print('\nOK! Digital Work Environment correctly set-up.')


def print_env_variables(env_variables):
    print('Environment Variables:\n')
    for p in env_variables:
        print('{name}: {value}'.format(
            name=p.name, value=p.value))


def print_hardware_info(hw_info):
    print('Hardware Information:\n')
    for n, v in hw_info.items():
        if type(v) is dict:
            print(n)
            pprint.pprint(v)
        elif type(v) is list:
            print(n)
            for nn in v:
                print('{}'.format(nn))
        else:
            print('{}\t{}'.format(n, v))


def summary_analysis():
    print('Summary Analysis for {node} - {user} '.format(
        node=platform.node(),
        user=getpass.getuser()))

    print('Retrieving information - please wait...\n')

    installed_prog = installed_prog_locations(prog_dict=PROGRAMS)

    print('iedl_checks version: {}\n'.format(__version__))
    print_basic_info()
    print('\n')
    print_installed_programs(installed_prog)


def full_analysis():
    print('Full Analysis for {node} - {user}\n '.format(
        node=platform.node(),
        user=getpass.getuser()))

    print('Retrieving information - please wait...\n')

    installed_prog = installed_prog_locations(prog_dict=PROGRAMS)
    env_variables = get_environment_variables(
        env_var_list=ENVIRONMENT_VARIABLES.get(
            platform.system(), ENVIRONMENT_VARIABLES['linux']))
    system_info = get_system_info()

    print('iedl_checks version: {}\n'.format(__version__))
    print_basic_info()
    print('\n')
    print_installed_programs(installed_prog)
    print('\n')
    print_env_variables(env_variables)
    print('\n')
    print_hardware_info(system_info)


def _argparser():
    """ Parse command line arguments
    """
    parser = argparse.ArgumentParser(
        description=('IEDL checks - '
                     'checks for the correct setup in the IndEcol group.'),
        epilog='Version: {}'.format(__version__))

    parser.add_argument('-s', '--summary', '-c', '--check',
                        action='store_true',
                        help=('Short summary of installed programs '
                              'and hardware.'))
    parser.add_argument('-f', '--full',
                        action='store_true',
                        help=('Full overview of installed programs, '
                              'environment variables and hardware.')
                        )

    args = parser.parse_args()
    return args


def cli_call():
    """ Entry point for the command line use
    """
    args = _argparser()
    if args.full:
        full_analysis()
    else:
        summary_analysis()


if __name__ == "__main__":
    try:
        cli_call()
    except Exception as e:
        print('\nERROR - SOMETHING WENT WRONG\n\n'
              '\tPlease email the output below to the Digital Lab:\n'
              '\nError message:\n{}'.format(e)
              )
