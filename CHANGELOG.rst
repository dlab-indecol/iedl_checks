20181115 - v0.1.2
##################

* Changed OK and Warning message (fix #3)
* fixed install_requires in setup.py (fix #2)
* fixed old system_checks message (fix #1)

20181112 - v0.1.1
##################

* Updated readme
* Changed version checking

20181112 - v0.1 - Initial version
###################################

Initial version

* First running version
* Checks for git, python, conda, matlab
* Some basic hardware checks
