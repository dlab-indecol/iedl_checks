""" Some simple tests

Extended tests can not be written b/c
the program test the underlying system
configuration.

Currently we only test for the
correct return type.
"""

import os
import sys


TESTPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(TESTPATH, '..'))

import iedl_checks.checks as icc   # NOQA


TEST_PROGRAMS = {
    'Standard python': {
        'cmd': 'python',
        'cmd_version': 'python --version',
            }
}


def test_system_info():
    assert type(icc.get_system_info()) is dict


def test_installed_prog():
    inst_prog = icc.installed_prog_locations(TEST_PROGRAMS)
    assert type(inst_prog) is list
    assert inst_prog[0].name in TEST_PROGRAMS.keys()


def test_env_var():
    assert type(icc.get_environment_variables(
        icc.ENVIRONMENT_VARIABLES)) is list
