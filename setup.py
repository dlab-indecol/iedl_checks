from setuptools import setup
from os import path

# read the contents of the README file
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

exec(open('./iedl_checks/version.py').read())

setup(
    name='iedl_checks',
    description=(
        'IEDL checks - '
        'checks for the correct setup in the IndEcol group.'),
    license='BSD 3-Clause License',
    long_description=long_description,
    url='https://gitlab.com/dlab-indecol/iedl_checks',
    author='Konstantin Stadler',
    author_email='konstantin.stadler@ntnu.no',
    version=__version__,  # noqa
    packages=['iedl_checks', ],
    package_data={'iedl_checks': ['./LICENSE']},
    entry_points={
        'console_scripts':
        ['iedl_checks = iedl_checks.checks:cli_call']},
    install_requires=['psutil >= 5.4.0'],
    classifiers=[
          'Programming Language :: Python :: 3.5',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3 :: Only',
          'License :: OSI Approved :: BSD 3-Clause License',
          'Development Status :: 0 - Beta',
          'Environment :: Console',
          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Developers',
          'Intended Audience :: Science/Research',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Scientific/Engineering',
          'Topic :: Utilities',
          ],
)
